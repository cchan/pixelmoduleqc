This program will allow the user to take IV curves and analyze the data.

To take an IV curve, the power supply voltage is run in constant current with a voltage limit and steps through different current values.

The slope and the offset of the IV curve is then fit to the data.

### Contents
1. [Description of the code](#description-of-the-code)
  * [Input JSON file](#input-json-file)
  * [Python script](#python-script)
2. [Taking IV curves](#taking-iv-curves)

----

# Description of the code

The IV curve code consists of one program `makeIVCurve.py` and a json file for input parameters `ivcurvescan.json`

### Input JSON file
The `*_ivcurvescan.json` sets all the parameters used to make IV curves. The parameters in `*_ivcurvescan.json` are split into 2 categories:
- `labRemote`: all the power supply settings
    - All commands assume a base command that sets up the path to the `labRemote` command
    - `readVoltageCmd`: command to read the voltage
    - `readCurrentCmd`: command to read the current
    - `setVoltCmd`: command to set the PS voltage.
    - `powerOnCmd`: command to turn on the PS.
    - `powerOffCmd`: command to turn off the PS. 
    - `powercycle`: set to 1 if you would like to powercycle the PS between each IV curve step
- `settings`: settings for the IV curve data taking
    - `NRepititons`: how many IV curves you would like to take
    - `start`: start current (minimum suggested start current is 0.1 A)
    - `end` : end current for the ramp. Max end current for SCC is 2.0 A, max end current for Triplet is 4.5A
    - `nSteps`: how many current values to go through for your IV curve
    - `sleep`: how long to wait after you set the PS current and voltage before configuring the chip or reading back the PS values
    - `chipType`: what kind of chip (RD53a, RD53b)
    - `chipName`: chip name from production database
    - `boardtype`: SCC or triplet? Used to determine if the user is inputting too high of a current
    - `outFile`: name of output file to store the IV curve data
    - `debug`: set to 1 if you want debug statements printed out

### Python script
The python script used to take IV curves is called `makeIVcurve.py`.

The code checks the max voltage and the current endpoints to ensure that they are not set to too high values.

It then configures the PS voltage and current with the values specified by the user. To ensure that the PS is in `constant current`, the voltage limnit should be set higher than the chip consumed voltage.

The PS current and voltage are then read back and stored for the future analysis.

Only the current value is changed during the data taking. 

If you choose to configure the chip, the return status of the configuration will be saved. The status is `0` if the chip has successfully configured. This will tell you at what minimum current you are able to configure the chip.

After running, there will be output json file(s) which store the the voltage, current, and if the chip configured (called `Status`). 


# Taking IV curves

The script to take IV curves is called `makeIVcurve.py`. The script can be run either using `python` or `python3`.
<pre>
python makeIVcurve.py
</pre>

