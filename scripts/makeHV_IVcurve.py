# Code to take IV curves using settings from a json
# Output the current and voltage to a json
# Author: Elodie Resseguie 

import os, subprocess
import json
import time

#open and read in JSON files
filename='hv_ivcurvescan.json'
f = open(filename, 'r')
datastore = json.load(f)
        
# get IV curve parameters
settings=datastore['settings']
if not(settings['NRepititons'] == None): nrep = settings['NRepititons']
if not len(settings['boardtype']) ==0: brd = settings['boardtype']
if not(settings['start'] == None): start = settings['start']
if not settings['end'] == None: 
    end = settings['end']

if not(settings['debug'] == None): debug = settings['debug']

if not settings['nSteps'] ==None: step = (end-start)/float(settings['nSteps'])
if not len(settings['boardtype']) ==0: brd = settings['boardtype']
if not len (settings['chipType']) == 0: chipType = settings['chipType']
if not len(settings['chipName']) == 0: chipName = settings['chipName']
if not settings['sleep'] == None: sleep = settings['sleep']
if not len(settings['outFile']) ==0: outFile = settings['outFile']

# get lab remote parameters
labremote=datastore['labRemote']

# configure default Yarr and labRemote commands
readVoltageCmd = labremote['readVoltageCmd']
readCurrentCmd = labremote['readCurrentCmd']
setVoltCmd = labremote['setVoltCmd']
powerOnCmd = labremote['powerOnCmd']
powerOffCmd = labremote['powerOffCmd']

# Start taking IV curve with specs defined above

# create output list to store in json  
voltage = []
current = []
chipStatus = []

count = start

powerOnCmd
while (count <= end):
                
    # set the PS voltage and current
    print('Setting the PS voltage %0.2f'%(count))
    os.system(setVoltCmd + ' ' +str(count))
    time.sleep( sleep ) 

    # read PS current and voltage     
    os.popen(readCurrentCmd)
    for i in range (0, nrep):
        pvolt = os.popen(readVoltageCmd)
        tmp_volt = pvolt.read().split('\n')[0]
        voltage.append(float(tmp_volt))
        pcurr = os.popen(readCurrentCmd)
        tmp_curr = pcurr.read().split('\n')[0]
        current.append(float(tmp_curr))
        if debug: print ('current: ' + tmp_curr + ', voltage: ' + tmp_volt)

    # increment the current
    count = count + step
        
# write to JSON file
with open('../results/'+outFile+'.json', 'w') as fout:
    json.dump({'chipType': chipType, 'scanType': 'IVcurve', 'chipName': chipName, 'values':{'Voltage': voltage, 'Current': current}}, fout, indent=2, sort_keys=True, separators=(',',':'))

# turn off PS at the end of the IV curves
if debug: print("Turning off the PS, IV curves are done")
os.system(powerOffCmd)


